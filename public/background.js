

chrome.runtime.onInstalled.addListener(() => {
    console.log('onInstalled...');
    // getAllTabsUrl();
    getUpcomingClass();
    // create alarm after extension is installed / upgraded
    chrome.alarms.create('getUpcomingClass', {
        periodInMinutes: 1
    });
    chrome.alarms.create('getAllTabsUrl', {
        periodInMinutes: 1
    });
});

chrome.alarms.onAlarm.addListener((alarm) => {
    switch (alarm.name) {
        case 'getUpcomingClass':
            getUpcomingClass();
            break;
        case 'getAllTabsUrl':
            // getProductsPopUp();
            getAllTabsUrl();
            break;
        default:
            console.log("Something is Wrong");
    }
});

// chrome.alarms.onAlarm.addListener((alarm) => {
//     getUpcomingClass();
// });
let courseType = "";
chrome.tabs.query({
    active: true,
    currentWindow: true
}, function (tabs) {
    var tabURL = tabs.length && tabs[0].url;
    console.log(tabURL);
});
let itemStatus = {};

function getCookies(domain, name, callback) {
    chrome.cookies.getAll({
        "url": domain,
    }, function (cookies) {
        if (callback) {
            callback(cookies);
        }
    });
}

function getUpcomingClass() {
    getCookies("https://math-stage.whjr.one/", "tk", (cookies) => {
        let token = cookies.find(el => el.name === "tk").value;
        let studentId = cookies.find(el => el.name === "ajs_user_id").value.replaceAll(/%22/g, "");
        console.log('cookies', cookies);
        fetch(`https://stage-api.whjr.one/api/V1/bookings/students/${studentId}/getClasses`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        })
        .then(resp => {
                resp.json().then(res => {
                    const nextClassData = res.data.nextClass;
                    console.log('response', nextClassData);
                    if (nextClassData) {
                        const startTime = new Date(nextClassData.startTime);
                        let currentTime = new Date();
                        let minutes = (startTime - currentTime) / (1000 * 60);
                        courseType = nextClassData.course_class.course.courseType;
                        if (minutes <= 10 && minutes >= 0) {
                            createNotfication(`Your Upcoming Class for ${nextClassData.course_class.course.courseType}`, `Starts in ${Math.round(minutes)} minutes, please join`, "joinClassNotification", [
                                { title: "Join Class" }, { title: "Ignore" }
                            ])
                        } else if (minutes <= 60) {
                            createNotfication(`Your Upcoming Class for ${nextClassData.course_class.course.courseType}`, `Is due next in ${Math.round(minutes)} minutes`, "upcomingClassNotification", [
                                { title: "Go to dashboard" }, { title: "Snooze" }
                            ])
                        }
                    }
                })
        })
    })
}

const createNotfication = (title, message, id, button) => {
    const options = {
        type: 'basic',
        iconUrl: '/icon.png',
        title: title,
        message: message,
        priority: 1,
        buttons: [...button],
        isClickable: true
    };
    chrome.notifications.create(id = id, options, function (data) {
        console.log(options);
    });
}
chrome.notifications.onButtonClicked.addListener((notificationsId, buttonIndex) => {
    console.log(`notifications ${notificationsId}`);
    if (notificationsId.includes("joinClassNotification")) {
        switch (buttonIndex) {
            case 0:
                console.log("You are getting redirected to class");
                chrome.tabs.create({ url: `https://${courseType === "CODING" ? "code": courseType.toLowerCase()}-stage.whjr.one/s/joinClass` });
                break;
            case 1:
                console.log("Button Ignore", buttonIndex);
                break;
            default:
                console.log("Something is Wrong");

        }
    }
    if (notificationsId.includes("upcomingClassNotification")) {
        switch (buttonIndex) {
            case 0:
                console.log("You are getting redirected to class");
                chrome.tabs.create({ url: `https://${courseType === "CODING" ? "code": courseType.toLowerCase()}-stage.whjr.one/s/dashboard` });
                break;
            case 1:
                console.log("Button Ignore", buttonIndex);
                break;
            default:
                console.log("Something is Wrong");

        }
    }
});


function getProductsPopUp() {
    chrome.tabs.create(
        {
          url: chrome.extension.getURL("index.html"),
          active: false
        },
        function(tab) {
          // After the tab has been created, open a window to inject the tab
          chrome.windows.create(
            {
              tabId: tab.id,
              type: "popup",
              focused: true
              // incognito, top, left, ...
            },
            function() {
              console.log('Done');
              
            }
          );
        }
      );

}

function getAllTabsUrl() {
    let urls = []
    chrome.windows.getAll({
        populate: true
    }, function (windows) {
        windows.forEach(function (window) {
            window.tabs.forEach(function (tab) {
                console.log(tab.url);
                // chrome.tabs && chrome.tabs.executeScript(null,{
                //             file: "notifyAboutCoursePopup.js"
                //         });
                // if(tab.url.includes("music")){
                //     chrome.tabs && chrome.tabs.executeScript(null,{
                //         file: "showMusicpop.js"
                //       });
                // } else{
                //     chrome.tabs && chrome.tabs.executeScript(null,{
                //         file: "notifyAboutCoursePopup.js"
                //       });
                // }
            });
        });
    });

    localStorage.setItem('tabUrls', urls)

    chrome.tabs.getSelected(null, function (tab) {
        tabUrl = tab.url;
        // console.log(`current: ${tabUrl}`);
    });

    chrome.tabs.onUpdated.addListener(function (params) { });

    chrome.tabs.onCreated.addListener(function (params) { });
    chrome.tabs.onActivated.addListener(function (params) { });
}