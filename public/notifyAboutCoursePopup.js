

var img=document.createElement("img");
img.style.zIndex="999999999";
img.style.position="fixed";
img.style.right="100px";
img.style.bottom="10px";

let mathWords = ["math", "mathematics", "pythagoras","rectangle","area","distance","length"];
let codeWords = ["code", "coding", "c++","java","python","javascript","web","webdevelop","developer","engineer","arrays","pointer"];
let musicWords =["music","piano","guitar","tambola","flute","damru","discover","sound","audio","tune","sheet","ting","tone","ringtone","marimba","sitar","harmonium","jingle","happy birthday"];
let isMathRelevant = mathWords.find(el => document.location.href.includes(el.toLowerCase()));
let isCodeRelevant = codeWords.find(el => document.location.href.includes(el.toLowerCase()));
let isMusicRelevant = musicWords.find(el => document.location.href.includes(el.toLowerCase()));

if(!document.location.href.includes("whjr.one")){
    if(isMathRelevant){
        var div=document.createElement("div");
        div.id="whjr-math-nudge";
    
        div.style.zIndex="999999999";
        div.style.position="fixed";
        div.style.right="100px";
        div.style.bottom="10px";
        
        var mathImage=document.createElement("img");
        var crossDiv=document.createElement("img");
        crossDiv.style.position="absolute";
        crossDiv.style.height="25px";
        crossDiv.style.width="25px";
        crossDiv.src="https://obj.whjr.one/8d1b24ba-a434-4f66-8320-2b12b14a9768.png";
        crossDiv.addEventListener('click',  () =>{
            document.querySelector("#whjr-math-nudge").replaceWith(null);
        })
        crossDiv.style.top="-25px";
        crossDiv.style.right="-25px";
        
        // mathImage.src="https://obj.whjr.one/05a34e70-41d8-4c3b-98d8-9743ada19e63.png";
        if (window.confirm("Go to WhitehatJr website ?")) {
            window.open("https://code-stage.whjr.one/s/math/trial/schedule", "Thanks for Visiting!");
          }
        mathImage.src="https://obj.whjr.one/36c35083-a3a1-4353-b7a9-81b824f9deb7.png";
        mathImage.style.height = "130px";
        mathImage.style.width = "400px";
        div.appendChild(crossDiv);
        div.appendChild(mathImage);
    
        document.body.appendChild(div); 
    } else if(isCodeRelevant){
        // img.src="https://obj.whjr.one/9e16ea2a-0c37-4fa2-a2b6-8beb28f118f3.png";
        var div=document.createElement("div");
        div.id="whjr-code-nudge";
    
        div.style.zIndex="999999999";
        div.style.position="fixed";
        div.style.right="100px";
        div.style.bottom="10px";
        
        var codeImage=document.createElement("img");
        var crossDiv=document.createElement("img");
        crossDiv.style.position="absolute";
        crossDiv.style.height="25px";
        crossDiv.style.width="25px";
        crossDiv.src="https://obj.whjr.one/8d1b24ba-a434-4f66-8320-2b12b14a9768.png";
        crossDiv.addEventListener('click',  () =>{
            document.querySelector("#whjr-code-nudge").replaceWith(null);
        })
        crossDiv.style.top="-25px";
        crossDiv.style.right="-25px";
        
        // mathImage.src="https://obj.whjr.one/05a34e70-41d8-4c3b-98d8-9743ada19e63.png";
        codeImage.addEventListener('click',() => {
            if (window.confirm("Go to WhitehatJr website ?")) {
                window.open("https://code-stage.whjr.one/s/code/trial/schedule", "Thanks for Visiting!");
              }
        })
        codeImage.src="https://obj.whjr.one/9e16ea2a-0c37-4fa2-a2b6-8beb28f118f3.png";
        codeImage.style.height = "130px";
        codeImage.style.width = "400px";
        div.appendChild(crossDiv);
        div.appendChild(codeImage);
        document.body.appendChild(div);
    } else if(isMusicRelevant){
        // https://obj.whjr.one/8d1b24ba-a434-4f66-8320-2b12b14a9768.png cross icon
        var div=document.createElement("div");
        div.id="whjr-music-nudge";
    
        div.style.zIndex="999999999";
        div.style.position="fixed";
        div.style.right="100px";
        div.style.bottom="10px";
        
        var musicImage=document.createElement("img");
        var crossDiv=document.createElement("img");
        crossDiv.style.position="absolute";
        crossDiv.style.height="25px";
        crossDiv.style.width="25px";
        crossDiv.src="https://obj.whjr.one/8d1b24ba-a434-4f66-8320-2b12b14a9768.png";
        crossDiv.addEventListener('click',  () =>{
            document.querySelector("#whjr-music-nudge").replaceWith(null);
        })
        crossDiv.style.right="0px";
        
        musicImage.src="https://obj.whjr.one/05a34e70-41d8-4c3b-98d8-9743ada19e63.png";
        musicImage.addEventListener('click',() => {
            console.log("I am redirecting");
            if (window.confirm("Go to WhitehatJr website ?")) {
                window.open("https://code-stage.whjr.one/s/music/trial/schedule", "Thanks for Visiting!");
              }
        })
        div.appendChild(crossDiv);
        div.appendChild(musicImage);
    
        document.body.appendChild(div); 
    }
    // document.body.appendChild(img); 
}
