import React from 'react'
import styled from "styled-components";

const Tiles = styled.div`
  width: 400px;
  height: 30px;
  background-color: #ddd;
  border-radius: 8px;
  position: relative;
  float: left;
  margin: 1em;
  opacity: 0.9;
  box-shadow: 0 2px 16px 1px rgba(33, 33, 33, 0.5);
  padding: 2em;
`
const NotificationTile = props => {
    return (
        <>
        <Tiles>
          {props.text}
        </Tiles>
        </>
    )
}

export default NotificationTile; 