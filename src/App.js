import logo from './logo.svg';
import './App.css';
import {useState, useEffect} from "react";
import { API_URL } from './constant';
import axios from "axios";
import NotificationTile from './common/NotificationTile';

function App() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [token,setToken] = useState("");

  const onSubmit = () =>{
    axios.post(`${API_URL}userDetail/authenticatePassword`,{emailOrMobile: email, password}).then(data => {
      console.log("data",data);
      setToken(data.data.data.token)
    }
    );
  }

  const handleChange = (event , setValue) =>{
    setValue(event.target.value)
  }

  // const dummyClick = () => {
  //     options = {
  //         type: "list",
  //         title: "Your Order Status",
  //         message: "Your order has been updated",
  //         items: '123',
  //         iconUrl: "/icon.png",
  //         eventTime: 8000
  //     };
  //     chrome.notifications.create(id = '', options, function (data) {
  //         console.log(data);
  //     });
  // }


  return (

    <div className="App">
      <NotificationTile text={"Welcome to WhitehatJr"}/>
      <NotificationTile text={"Dummy Notification: Next Class for Math is on 15 Aug, 2021"}/>
      <NotificationTile text={"Dummy Notification: Next Class for Coding is on 16 Aug, 2021"}/>
      <NotificationTile text={"Dummy Notification: Next Class for Music is on 17 Aug, 2021"}/>
      <NotificationTile text={"Dummy Notification: Next Class for Coding is on 18 Aug, 2021"}/>
      <NotificationTile text={"Dummy Notification: Next Class for Music is on 19 Aug, 2021"}/>
    </div>
  );
}

export default App;
